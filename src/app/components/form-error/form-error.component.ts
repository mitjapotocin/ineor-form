import { Component, Input } from '@angular/core';

@Component({
  selector: 'form-error',
  styleUrls: ['./form-error.component.scss'],
  template: `
    <div [class.show]="!!error" class="form-error">{{ error }}</div>
  `
})
export class FormErrorComponent {
  @Input() error: string = '';
}