import { tap } from 'rxjs/operators';
import { Component } from '@angular/core';
import { map } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { GimpyApiResponse, GimpyData } from 'src/app/models/gimpy-api.model';

@Component({
  selector: 'home',
  styleUrls: ['./success.component.scss'],
  template: `
    <div class="wrapper">
      <h1>{{ title }}</h1>
      <img 
        *ngIf="gif?.url" 
        class="image-border" 
        [alt]="gif?.alt"
        [src]="gif?.url">
    </div>
  `
})
export class SuccessComponent {
  title = 'Appointment successfully booked';
  gif?: { url: string, alt: string };

  constructor(private _apiService: ApiService) { }

  ngOnInit() {
    this.getGifSrc()
  }

  getGifSrc() {
    this._apiService.getGifs()
      .pipe(
        map(({ data }: GimpyApiResponse) => this.getRandomGif(data)),
        tap((gif) => { this.gif = gif; })
      )
      .subscribe()
  }

  private getRandomGif(gifs: GimpyData[]): { url: string, alt: string } {
    let randomGif = gifs[Math.floor(Math.random() * gifs.length)];
    return {
      url: randomGif.images.original.url,
      alt: randomGif.title
    }
  }
}
