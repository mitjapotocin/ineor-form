import * as moment from 'moment';
import { catchError, debounceTime, filter, map, mergeMap, take, tap } from 'rxjs/operators';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EMPTY, fromEvent, Subscription, zip } from 'rxjs';
import { Appointment } from 'src/app/models/appointment.model';
import { BarberService } from 'src/app/models/barber-service.model';
import { Barber, WorkHours } from 'src/app/models/barber.models';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { FormPlaceholders, PLACEHOLDERS_COPY } from './placeholders';

@Component({
  selector: 'booking-form',
  templateUrl: './booking-form.component.html',
  styleUrls: ['./booking-form.component.scss']
})
export class BookingFormComponent implements OnInit, OnDestroy {

  barbers: Barber[] = [];
  services: BarberService[] = [];
  appointments: Appointment[] = [];
  showAllErrors: boolean = false;
  placeholders: FormPlaceholders = PLACEHOLDERS_COPY.desktop;

  form = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    email: new FormControl<number | null>(null, [
      Validators.required,
      Validators.email
    ]),
    contactNumber: new FormControl<number | null>(null, [
      Validators.required,
      Validators.pattern(/^[0-9]*$/)
    ]),
    barber: new FormControl<Barber | null>(null, [Validators.required]),
    service: new FormControl<BarberService | null>(null, [Validators.required]),
    date: new FormControl('', [Validators.required]),
    time: new FormControl('', [Validators.required]),
  });

  resizeSub?: Subscription;

  constructor(
    private _apiService: ApiService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.setPlaceholders()
    this.getData();
    this.subscribeToResize();
  }

  ngOnDestroy() {
    this.resizeSub?.unsubscribe();
  }

  get todayDate() {
    return new Date().toISOString().split('T')[0]
  }

  get servicePrice(): string {
    let servicePrice = this.form.value?.service?.price

    return servicePrice
      ? `Price is ${servicePrice} €`
      : this.placeholders.price;
  }

  areInputsInvalid(inputs: string[] | string): boolean {
    return [inputs]
      .flat()
      .map((input) => this.form.get(input))
      .filter((input) => input?.touched || this.showAllErrors)
      .some((input) => input?.invalid)
  }

  onSubmit() {
    if (this.form.invalid) {
      this.showAllErrors = true;
      return;
    }
    if (this.isDateBeforeNow()) {
      this.handleNotAvailable('Date is before now');
      return;
    }

    this._apiService.getAppointments()
      .pipe(
        /**
         * Catch api error
         */
        catchError((err) => {
          this.handleApiError(err);
          return EMPTY;
        }),
        take(1),
        /**
         * Assign appointments
         */
        tap((appointments: Appointment[]) => { this.appointments = appointments; }),
        /**
         * Check availability
         */
        map(() => this.checkIfBarberIsAvailable()),
        /**
         * Display warning if barber is not available
         */
        filter((isAvailable: boolean) => {
          if (!isAvailable) { this.handleNotAvailable(''); }
          return isAvailable
        }),
        /**
         * Post appointment
         */
        mergeMap(() => this._apiService.postAppointment(this.formToAppointmentModel())),
        /**
         * Catch api error
         */
        catchError((err) => {
          this.handleApiError(err);
          return EMPTY;
        }),
        /**
         * Reroute so success page
         */
        tap(() => { this._router.navigate(['/success']); })
      )
      .subscribe();
  }

  private isDateBeforeNow(): boolean {
    let { date, time } = this.form.value;
    return moment(date + ', ' + time, 'YYYY-MM-DD, HH:mm').isBefore(moment());
  }

  private handleNotAvailable(alert?: string) {
    /**
     * This should be handled differently,
     * but it is outside of the scope for this task.
     */
    if (!alert) {
      alert = 'Barber is not available at this time. See console output for more info about barbers work hours and appointments.';
    }

    console.log('Barbers:', this.barbers);
    console.log('Appointments:', this.appointments);

    window.alert(alert);
  }

  subscribeToResize() {
    this.resizeSub = fromEvent(window, 'resize')
      .pipe(
        debounceTime(50),
        tap(() => { this.setPlaceholders(); })
      )
      .subscribe();
  }

  setPlaceholders() {
    this.placeholders = (window.innerWidth > 768)
      ? PLACEHOLDERS_COPY.desktop
      : PLACEHOLDERS_COPY.mobile;
  }

  private getData() {
    zip(
      this._apiService.getBarbers(),
      this._apiService.getServices()
    )
      .pipe(
        catchError((err) => {
          this.handleApiError(err);
          return EMPTY;
        }),
        take(1),
        tap(([barbers, services]: [Barber[], BarberService[]]) => {
          this.barbers = barbers;
          this.services = services;
        })
      )
      .subscribe();
  }

  private handleApiError(err: any) {
    console.log(err);
    window.alert('Oops! Something went wrong. May you forgot to run json-server?');
  }

  private formToAppointmentModel(): Appointment {
    let { barber, service, date, time } = this.form.value;

    return {
      barberId: (barber as Barber).id,
      serviceId: (service as BarberService).id,
      startDate: moment(date + ', ' + time, 'YYYY-MM-DD, HH:mm').unix(),
    }
  }

  private checkIfBarberIsAvailable(): boolean {
    let { barber, service, date, time } = this.form.value;

    if (!(barber && service && date && time)) {
      throw new Error('Form fields can not be nill');
    }

    let appointmentStartTime = moment(date + ', ' + time, 'YYYY-MM-DD, HH:mm');
    let appointmentEndTime = moment(appointmentStartTime.valueOf()).add(service.durationMinutes, 'minutes');

    let day = appointmentStartTime.day();
    let barberWorkHours = barber.workHours.find((workHours) => workHours.day === day) as WorkHours;

    if (!barberWorkHours) { return false; }

    return this.isAppointmentWithinWorkHours(appointmentStartTime, appointmentEndTime, barberWorkHours)
      && !this.isAppointmentOverlappingWithOtherAppointments(appointmentStartTime, appointmentEndTime);
  }

  private isAppointmentOverlappingWithOtherAppointments(
    appointmentStartTime: moment.Moment,
    appointmentEndTime: moment.Moment
  ): boolean {
    return this.appointments.some((appointment) => {
      let serviceDuration = this.services
        .find((service) => service.id === appointment.serviceId)?.durationMinutes;

      let _appointmentStartTime = moment.unix(appointment.startDate);
      let _appointmentEndTime = moment(_appointmentStartTime.valueOf()).add(serviceDuration, 'minutes');

      return this.areRangesOverlapping(appointmentStartTime, appointmentEndTime, _appointmentStartTime, _appointmentEndTime);
    })
  }

  private isAppointmentWithinWorkHours(
    appointmentStartTime: moment.Moment,
    appointmentEndTime: moment.Moment,
    { startHour, endHour, lunchTime }: WorkHours
  ): boolean {
    let workStartTime = moment(appointmentStartTime.valueOf()).hour(startHour).minute(0);
    let workEndTime = moment(workStartTime.valueOf()).hour(endHour).minute(0);

    let isOutsideOfWorkingHours = appointmentStartTime.isBefore(workStartTime) || appointmentEndTime.isAfter(workEndTime);

    let lunchTimeStart = moment(appointmentStartTime.valueOf()).hour(lunchTime.startHour).minute(0);
    let lunchTimeEnd = moment(lunchTimeStart.valueOf()).add(lunchTime.durationMinutes, 'minutes');

    let isAppointmentBetweenLunch = this.areRangesOverlapping(appointmentStartTime, appointmentEndTime, lunchTimeStart, lunchTimeEnd);

    return !(isOutsideOfWorkingHours || isAppointmentBetweenLunch);
  }

  private areRangesOverlapping(
    range1Start: moment.Moment,
    range1End: moment.Moment,
    range2Start: moment.Moment,
    range2End: moment.Moment
  ): boolean {
    return (
      range2Start.isBetween(range1Start, range1End)
      || range2End.isBetween(range1Start, range1End)
      || range1Start.isBetween(range2Start, range2End)
      || range1End.isBetween(range2Start, range2End)
      || range1Start.isSame(range2Start)
      || range1End.isSame(range2End)
    )
  }
}
