export interface PlaceholdersCopy {
  desktop: FormPlaceholders;
  mobile: FormPlaceholders;
}
export interface FormPlaceholders {
  firstName: string;
  lastName: string;
  email: string;
  contactNumber: string;
  barber: string;
  service: string;
  date: string;
  time: string;
  price: string;
  cta: string;
}

export const PLACEHOLDERS_COPY: PlaceholdersCopy = {
  desktop: {
    firstName: 'First Name',
    lastName: 'Last Name',
    email: 'Email',
    contactNumber: 'Contact number',
    barber: 'Select barber',
    service: 'Select service',
    date: 'Select date',
    time: 'Select time',
    price: 'Select any service',
    cta: 'Book appointment'
  },
  mobile: {
    firstName: 'First Name',
    lastName: 'Last Name',
    email: 'Email',
    contactNumber: 'Phone',
    barber: 'Barber',
    service: 'Service',
    date: 'Date',
    time: 'Time',
    price: 'Price',
    cta: 'Book'
  }
}
