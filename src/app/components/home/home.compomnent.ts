import { Component } from '@angular/core';

@Component({
  selector: 'home',
  styleUrls: ['./home.component.scss'],
  template: `
    <h1>{{ title }}</h1>
    <p class="subtitle">{{ subTitle }} <span>{{ subTitleExtra }}</span></p>

    <img 
      [src]="coverImage" 
      class="image-border"
      alt="Barber">
    <booking-form></booking-form>
  `
})
export class HomeComponent {
  title: string = 'Book your barber';
  subTitle: string = "Great Hair Doesn’t Happen By Chance. It Happens By Appointment!";
  subTitleExtra: string = "So, Don't Wait And Book Your Appointment Now!";
  coverImage: string = 'assets/images/cover-image.jpg';
}
