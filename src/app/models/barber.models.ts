export interface WorkHours {
  id: number,
  day: number,
  startHour: number,
  endHour: number,
  lunchTime: { startHour: number, durationMinutes: number }
}

export interface Barber {
  id: number;
  firstName: string,
  lastName: string,
  workHours: WorkHours[]
}