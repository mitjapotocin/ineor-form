export interface BarberService {
  id: number,
  name: 'Shave' | 'Haircut' | 'Shave + Haircut',
  durationMinutes: number,
  price: string
}
