export interface Appointment {
  id?: number,
  barberId: number,
  startDate: number,
  serviceId: number
}
