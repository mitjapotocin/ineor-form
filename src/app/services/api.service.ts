/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Barber } from '../models/barber.models';
import { Appointment } from '../models/appointment.model';
import { BarberService } from '../models/barber-service.model';
import { GimpyApiResponse } from '../models/gimpy-api.model';

const API_URL = 'http://localhost:3000';

@Injectable()
export class ApiService {

  constructor(
    private _http: HttpClient
  ) { }

  getBarbers(): Observable<Barber[]> {
    let url = API_URL + '/barbers';

    return this._http.get(url, { responseType: 'json' }) as Observable<Barber[]>;
  }

  getAppointments(): Observable<Appointment[]> {
    let url = API_URL + '/appointments';

    return this._http.get(url, { responseType: 'json' }) as Observable<Appointment[]>;
  }

  getServices(): Observable<BarberService[]> {
    let url = API_URL + '/services';

    return this._http.get(url, { responseType: 'json' }) as Observable<BarberService[]>;
  }

  postAppointment(appointment: Appointment): Observable<BarberService[]> {
    let url = API_URL + '/appointments';

    return this._http.post(url, appointment, { responseType: 'json' }) as Observable<BarberService[]>;
  }

  getGifs(): Observable<GimpyApiResponse> {
    let url = 'http://api.giphy.com/v1/gifs/search?api_key=KeTn0RgXZQF8EDkUGgQmSaJYuWPEz5mI&q=barber'

    return this._http.get(url, { responseType: 'json' }) as Observable<GimpyApiResponse>;
  }
}
